%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Auteur : Stéphane CARPENTIER
% Fichier : comparaison.tex
% 	Modif : sam. 04 janv. 2020 19:36
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\part{Comparaison entre Word et \LaTeX{}}
\section{Un peu de typographie}
Je ne vais pas m'y attarder parce que la littérature montrant la supériorité de \LaTeX{} sur Word est abondante.
De plus, personne ne s'intéresse à la typographie, sinon, Word ne serait pas aussi mal utilisé.
Cependant, le but ultime d'un document est d'être lu.
Et c'est une bonne typographie qui permet à un document d'être lu dans les meilleurs conditions possibles.
Je ne peux donc pas passer ce point sous silence.

D'abord, la bonne typographie ne se remarque pas.
C'est ce que les gens ont souvent du mal à comprendre.
C'est pour ça qu'il y a tant de documents avec des parties soulignées ou en gras à tort et à travers.
Le pire est la partie à la fois soulignée et en gras.
Ce que l'œil remarque, ce sont les erreurs de typographie.
Quand il n'y a pas d'erreur de typographie, l'œil lit de façon optimale et ne remarque rien.
Même si, après avoir lu des milliers de pages wordiques rédigées n'importe comment, vous avez l'impression inverse.

Tenez ce document à bout de bras et regardez le de loin.
Vous devez voir des blocs homogènes, avec les titres qui ressortent et deux parties.
L'une des deux parties que vous devez voir est la partie soulignée.
\uline{Le soulignement avec \LaTeX{}, contrairement au soulignement wordique, ne coupe pas les jambages}.
Le soulignement wordique est tellement nul et répandu qu'une légende se répand de plus en plus.
La légende veut que le soulignement soit typographiquement mauvais.
C'est faux, c'est le mauvais soulignement, ou le soulignement dans de mauvaises conditions qui est mauvais.
L'autre partie que vous devez voir est la partie en gras.
\textbf{La mise en gras avec \LaTeX{} doit être sensiblement égale à celle avec Word.}
Par contre, vous ne devez pas remarquer la partie en italique.
\Emphase{La partie en italique ne se remarque que quand vous la lisez.}
De même, vous ne voyez pas «~la partie entre guillemets~» avant de la lire.

Le paragraphe précédant était typographiquement mauvais pour montrer mon point.
L'œil ne lit pas les lettres une par une.
Toutes les études sont unanimes là-dessus.
L'œil lit des parties autour de la lettre à lire.
Si, au dessus ou en dessous de la partie à lire, il y a des parties qui ressortent, son attention est détournée et il a plus de mal à lire.
Si vous voyez une partie en gras ou soulignée lorsque vous tenez un texte à bout de bras, lorsque vous lirez la phrase au dessus ou en dessous, l'œil la verra.
Si une partie est soulignée sur un texte de mille pages, vous ne fatiguerez pas vraiment plus.
Mais si presque la moitié des mots est en gras ou soulignée alors, vous fatiguerez plus sans savoir pourquoi.
Le gras et le souligné ne sont pas faits pour être lus au fil de l'eau mais pour être trouvés facilement.
Par exemple, dans un dictionnaire, vous devez voir les mots directement pour les trouver facilement.
De même que les titres doivent ressortir pour pouvoir trouver rapidement la partie intéressante.
C'est pour ne pas perturber l'œil que les titres sont séparés du reste du texte.
Ils doivent ressortir pour être remarqués rapidement, mais ne doivent pas gêner la lecture du paragraphe.
Puisque vous ne me croyez pas, lisez cette partie :

Je suis en retard.
Je vois mon bus qui arrive.
Je vais avoir du mal à l'avoir.
Il faut que je coure.
Je cours.
Le plus vite possible.
Il s'arrête.
Je suis encore loin.
Je n'arrive plus à respirer.
Mais je dois accélérer.
Il m'a vu.
Il m'attend.
Je l'ai eu de justesse, c'est bon, maintenant je peux me reposer, il faut que je reprenne ma respiration, mais je n'y arrive pas, je suis essoufflé.

Maintenant, lisez ça :

Je suis en retard, je vois mon bus qui arrive, je vais avoir du mal à l'avoir, il faut que je coure, je cours, le plus vite possible, il s'arrête, je suis encore loin, je n'arrive plus à respirer, mais je dois accélérer, il m'a vu, il m'attend, je l'ai eu.
De justesse.
C'est bon.
Maintenant, je peux me reposer.
Il faut que je reprenne ma respiration.
Mais je n'y arrive pas.
Je suis essoufflé.

Vous avez vu la différence ?
Dans le premier cas vous lisez lentement lorsque l'action est rapide et rapidement lorsqu'elle est lente.
Dans le second cas, votre lecture est plus en adéquation avec l'action.
La seule différence entre les deux textes est la ponctuation.
Vous ne le remarquez pas, mais votre œil est attentif à beaucoup de choses.
Vous avez lu tellement de documents wordiques que vous ne remarquez même plus les aberrations.
Vous êtes même capable de trouver des qualités à des erreurs.
Par exemple, si vous trouvez que mes marges sont trop larges, ça vient d'une perturbation wordique qui incite à faire les lignes les plus longues possibles.
Lorsque vous lisez, si la ligne est trop longue, votre œil fatigue.
Ensuite, à la fin de la ligne, il a du mal à retrouver le début de la ligne suivante.

Lorsque les espaces entre les mots sont trop variés, votre œil est gêné.
Et ça, c'est le premier et plus gros reproche fait à Word.
Il fait n'importe quoi au niveau de l'espacement : ce n'est pas son problème.
Si le dernier mot ne peut par rentrer sur la ligne, il le met à la ligne suivante.
Ensuite, il espace les mots de la ligne en conséquence.
Mais il ne se demande pas si de descendre le mot de la ligne du dessus permettrait un rendu plus homogène.
Alors que \LaTeX{} a un algorithme de calcul très compliqué sur lequel il prend l'affichage de la page en compte.
Il est même capable de modifier la largeur des caractères (de moins de deux pourcents c'est invisible à l'œil) pour améliorer le rendu.
C'est de la micro-typographie que Word ne gère absolument pas.
Au niveau micro-typographie, il y a autre chose que j'aime.
\LaTeX{} me le permet mais pas Word.
Regardez la fin des lignes de ce texte qui finissent par un signe de ponctuation.
Le signe est un peu dans la marge.
Comme il est plus petit qu'une lettre, la fin des lignes est moins droite, mais l'œil a l'impression inverse.

Il y a autre chose que Word ne gère pas, mais qu'il donne l'impression de permettre de gérer.
En fait, c'est tellement compliqué à gérer avec Word que je considère que ça n'existe pas.
Ce sont les ligatures.
Regardez les «~f~»  dans «~souffle fier~».
Ils sont liés avec le «~l~» et le «~i~».
Ce n'est pas juste les caractères qui sont rapprochés, mais ce sont de nouveaux caractères.
Là, je ne parle que de différences que vous pouvez voir vous-même.
Mais il y en a beaucoup d'autres, la littérature sur le sujet est abondante.

Vous allez me dire que la typographie (micro ou pas) n'a aucune importance.
Permettez-moi de ne pas être d'accord.
Vous avez le droit de ne pas vous y intéresser.
De même que certaines personnes ne s'intéressent ni à la grammaire ni à l'orthographe.
Pour moi, une typographie correcte est un respect du lecteur.
Au même titre qu'une grammaire et qu'une orthographe soignées.
Même si le lecteur ne le remarque pas.
Pour moi, utiliser un logiciel qui m'oblige à avoir une typographie médiocre revient à utiliser un logiciel qui me rajoute des fautes de grammaire et d'orthographe.

Maintenant, je vais vous parler des logiciels que j'utilise pour avoir ce rendu.
Si la littérature est abondante sur ce que j'ai écrit au dessus, elle a un défaut.
C'est qu'elle donne l'impression que \LaTeX{} est plus dur à utiliser mais que ça vaut le coup pour la typographie.
Je vais donc vous montrer pourquoi ce n'est pas plus dur à utiliser.
C'est peut être plus dur à apprendre, mais je n'en suis pas si sûr.
Pour être honnête en comptant le temps d'apprentissage de Word, il faudrait ajouter le temps passé à s'y retrouver à chaque nouvelle version.
Alors que \LaTeX{} ne s'apprend qu'une fois.

Pour vous aider à mesurer la portée de cette dernière phrase, je me sent obligé de montrer quelques éléments de comparaison.
Le \TeX{}book a été écrit en 1984 et est, encore aujourd'hui, considéré comme la référence absolue pour celui qui veut maîtriser les arcanes intimes du logiciel (donc pas le débutant, ou alors le débutant vraiment motivé).
Je possède plusieurs livres sur \LaTeX{} qui ont été écrits entre 1994 et 2000.
Tous ces livres pourraient encore être très utiles à quelqu'un souhaitant découvrir \LaTeX{}.
Bien sûr, il y a eu des nouveautés apportées à \LaTeX{} qui n'étaient pas décrites lors de leurs rédactions.
Mais tout ce qui y est indiqué doit encore fonctionner sur une version de \LaTeX{} à jour.
Je n'ai jamais vu un livre sur Word qui conservait le moindre intérêt dès qu'une nouvelle version de Word sortait.
Si vous en connaissez un, dites-le moi.
Je serais très intéressé pour me le procurer, il pourrait me servir à rester calme au boulot.

\section{Présentation de \LaTeX{}}
D'abord, un mot sur sa prononciation.
Le «~x~» final vient du grec, il s'écrit généralement «~ch~» et se prononce «~k~» en français, comme dans technique.
En français, \LaTeX{} se prononce donc «~latèque~» et pas «~latèxe~».
Maintenant que je l'ai écrit, vous le prononcez comme vous le voulez, ça ne me gêne pas.

Word est un environnement de «~rédaction~» intégré.
C'est à dire que même si c'est nul et que plein de trucs ne vous plaisent pas dedans, vous devez tous les prendre.
Pour créer un document Word, vous devez ouvrir Word et tout faire dedans.
De la rédaction du document à son impression en passant par sa mise en page et sa correction orthographique.

Ce qui est fondamentalement différent de \LaTeX{}.
En effet, \LaTeX{} n'intervient qu'à la fin de la rédaction, à la phase qui s'appelle la compilation.
C'est à dire que \LaTeX{} prend un fichier avec tout plein de commandes en entrée et le transforme en un beau document prêt à être lu\Note{Je simplifie un peu, \LaTeX{} est surtout une surcouche de \TeX{}, mais avec les moteurs récents, la différence n'intéresse que les spécialistes.}.
Je ne veux pas dire que vous êtes obligé d'avoir écrit les deux mille pages de votre roman avant de le compiler.
Ce que je veux dire, c'est que pour obtenir votre document prêt à compiler, vous employez le moyen que vous voulez.
Dans la suite, j'appellerai le fichier qui contient tout plein de commandes le fichier source.
Le source est le document de travail, le document final est le document que vous distribuez à vos lecteurs.
Lorsque vous écrivez votre document pour \LaTeX{}, vous ne voyez donc pas votre document final.
Vous avez donc, de fait, une séparation entre le fond et la forme du document.
C'est quelque chose qui perturbe les non habitués.
Mais je montrerai plus loin en quoi c'est un atout une fois l'effet de surprise passé.

Voici un exemple de source en \LaTeX{} et de son rendu.

\begin{boilat}{Exemple de source et de son rendu}
Un tout premier exemple de texte \LaTeX{}.
Pour \emph{mettre un texte en emphase}.
Ou alors, pour faire tourner des textes : \rotatebox{15}{penché}.
\end{boilat}

Par exemple, vous voulez écrire «~\siecle{xx} siècle~» proprement.
Pour respecter les règles de typographie, le siècle est écrit en petites capitales et le «~e~» en exposant.
Avec Word vous écrivez «~xxe siècle~».
Vous sélectionnez les deux «~x~».
Vous farfouillez dans le menu pour trouver les petites capitales.
Vous cliquez pour mettre les «~x~» en petites capitales.
Vous sélectionnez le «~e~».
Vous farfouillez encore dans le menu pour trouver les exposants.
Vous cliquez pour mettre le «~e~» en exposant.
Vous pouvez passer à la suite.

Avec \LaTeX{}, rien de tout ça.
\begin{boilat}{Premier exemple de siècle}
Vous écrivez \textsc{xx}\ieme{} siècle et c'est tout.
\end{boilat}
Et là, vous trouvez que ce n'est pas génial.
Ce n'est effectivement pas génial, mais mes raisons sont différentes des vôtres, nous y reviendrons.
Vous êtes en train de vous dire que c'est plus simple avec Word.
Mais c'est seulement parce que vous ne connaissez pas \LaTeX{}.
Vous retombez dans votre travers de comparer l'utilisation de commandes que vous ne connaissez pas avec l'utilisation de Word que vous connaissez.
Pour quelqu'un comme moi qui connaît \LaTeX{} et pas Word, il est plus rapide d'écrire les commandes \LaTeX{} quand elles sont connues que de chercher au hasard dans les menus de Word.

Une grosse différence entre l'utilisation de \LaTeX{} et de Word est la façon d'appréhender les documents.
Dans les deux cas, lorsque vous commencez un nouveau document, vous avez une page blanche.
Mais avec Word, vous regardez les menus pour savoir ce qu'il vous propose.
Ensuite, vous cliquez en fonction de ce que vous trouvez bien.
Avec \LaTeX{}, vous devez commencer par vous demander ce que vous voulez.
En fonction de ce que vous voulez, vous allez utiliser les bonnes commandes pour l'obtenir.
Il y a des cas très tordus, mais en général, si vous savez ce que vous voulez, vous pouvez l'obtenir facilement avec \LaTeX{}.
Alors qu'avec Word, si vous voulez quelque chose qui diffère un peu des menus par défaut, ça peut être très dur à obtenir.
\LaTeX{} vous offre une liberté que Word ne permet pas.
Cette liberté a un coût, c'est que vous devez réfléchir à ce que vous voulez.
Dès le début, vous devez vous demander si vous voulez écrire une lettre, un CV, un article, un livre ou autre chose avec \LaTeX{}.
Beaucoup de gens préfèrent se laisser guider sans se poser de question, mais cette liberté est très importante pour moi.

Une fois que les documents sont en cours de rédaction, la différence dans la gestion des erreurs apparaît.
Lorsque je m'y prends mal avec Word, j'ai autre chose que ce que je veux.
Il essaye de faire ce que je veux, mais comme il n'y arrive pas, il me donne autre chose.
Il ne me prévient pas, bien sûr.
Je ne comprends pas pourquoi et je suis frustré.
Alors qu'avec \LaTeX{}, si je m'y prends mal, j'ai une grosse erreur de compilation.
Il va essayer de passer outre mon erreur et de me donner quelque chose quand même.
Mais au moins, je sais pourquoi il fait autre chose que ce que je veux et je peux le corriger.
Les erreurs de compilation font peur à certains, mais moi, elles me permettent de trouver mes erreurs.
Elles me permettent d'obtenir ce que je veux plus facilement.

Maintenant que nous avons vu les principes, nous allons pouvoir en étudier les avantages.
Le premier avantage, qui ne saute pas aux yeux de tout le monde, est que la phase de lecture et la phase de rédaction n'ont pas les mêmes contraintes.
Pour lire un livre dans les meilleurs conditions, il est préférable de l'avoir sous forme papier (Les liseuses ne sont pas encore très utilisées en France).
Alors que pour l'instant, seul l'écran permet de lire le document en cours de rédaction (la liseuse ne le permet pas non plus, mon argument n'est donc pas ébranlé par la liseuse).
Or, une feuille de papier n'a pas les mêmes caractéristiques qu'un écran.
Il est beaucoup plus reposant pour l'œil de lire en clair sur fond sombre.
Mais, si vous voulez imprimer un livre sur fond sombre, ça va vous coûter une fortune en cartouches d'encre.
Sans compter l'aspect écologique.
Avec Word, vous êtes obligé, de choisir un aspect visuel, il sera forcément utilisé pour la rédaction comme pour la lecture.
Alors qu'avec \LaTeX{}, je choisis un aspect visuel très différent pour la rédaction et pour la lecture.
Que ce soit le choix des couleurs ou celui des polices de caractères.
Alors que Word est néfaste pour mes yeux en m'obligeant à rédiger des documents dans de mauvaises conditions.

Un autre avantage est que je ne cherche pas à voir les mêmes choses pendant les phases de rédaction et de lecture.
Par exemple, quand je rédige, j'aime bien voir mes fautes de frappe.
Par contre, je n'apprécie pas trop qu'elles soient soulignées quand je fais lire mon texte.
Surtout que ce n'est pas forcément une faute, ça peut être une limitation du correcteur, ça fait néanmoins brouillon.
De même, lorsque je rédige, je peux avoir envie de mettre des commentaires à mon usage unique.
Par exemple, je peux indiquer qu'il faudra que je développe mieux un paragraphe plus tard.
Lorsque je fais lire mon texte, ces commentaires n'apparaissent pas, alors qu'avec Word, ils sont aussi envoyés au lecteur.
C'est bien une limitation de Word qui n'apparait pas avec \LaTeX{}.
Un autre exemple, c'est que quand je rédige, j'aime bien savoir d'un coup d'œil si mes phrases sont trop longues.
Avec \LaTeX{}, c'est simple, j'écris une phrase par ligne, j'aperçois instantanément la longueur de ma phrase.
\LaTeX{} se chargera tout seul de faire de beaux paragraphes.
Avec Word, je n'ai aucun moyen d'estimer les longueurs de mes phrases.
Allez, un dernier exemple pour la route.
Pour rédiger un gros document, il est souvent pratique d'utiliser des fichiers différents.
Alors que pour la lecture, il est plus agréable d'avoir tout ensemble.
J'ai bien entendu parler de fichiers Word sur plusieurs documents, mais je n'en ai jamais vu.
Soit c'est une légende, soit c'est tellement compliqué que seule une élite s'en sert.
Alors qu'avec \LaTeX{}, l'inclusion de fichiers est indiquée dans tous les manuels d'initiation que j'ai pu voir.

Si l'intérêt de la séparation des outils peut sembler théorique, il a vraiment des avantages concrets.
L'utilisation de Word, non seulement l'empêche pour certains cas, mais en plus ne l'incite pas quand c'est possible.

\section{Séparation entre le fond et la forme}

C'est un problème qui est souvent mal compris et donc trop souvent sous-estimé.
Lorsque vous rédigez un document, vous avez deux choses à traiter.
D'un côté, les mots que vous voulez utiliser, avec éventuellement des illustrations, c'est le contenu, ou le fond du document.
De l'autre côté, vous avez la façon dont ces mots et illustrations doivent s'afficher, c'est la présentation ou la forme du document.
Avec Word, vous ne pouvez pas y échapper, le fond et la forme sont emmêlés, vous ne pouvez pas les séparer.
Avec \LaTeX{}, comme votre document est composé de commandes, il est possible de séparer le fond de la forme, c'est même conseillé.
Revenons à notre siècle que nous voulions écrire correctement.
\begin{boilat}{Problème du siècle}
Quand j'écris \textsc{xx}\ieme{} siècle je ne sépare pas le fond de la forme.
\end{boilat}
C'est ça qui me gêne, pas le fait que ces commandes semblent complexes.
Pour séparer le fond de la forme, c'est très simple.
Il suffit de définir une commande \liglat{\siecle{}} dans le préambule du document.
\begin{boilat}{Bonne version du siècle}
Ensuite, dans le code du texte, il devient possible d'écrire \siecle{xx} siècle pour avoir le même résultat.
\end{boilat}
Et là, c'est bien, le fond et la forme sont séparés.
Les avantages qui découlent de cette séparation sont nombreux.

Le premier, c'est que lorsque je m'occupe de savoir ce que je dois écrire, je n'ai pas besoin de m'occuper de sa représentation.
Ça me permet de me concentrer sur mes phrases, je n'ai pas à m'inquiéter de savoir si le rendu me plait ou pas.
Quand je rédige, je ne suis pas perturbé par une mise en page qui pourrait ne pas me plaire.
Je m'occuperai de ma mise en page en temps voulu.
Au moment de la rédaction de ma commande \liglat{\siecle{}}, là je m'occupe de l'affichage.
Je ne suis pas perturbé par le contenu des paragraphes à me demander si ma phrase est bien tournée.
Je suis focalisé sur la rédaction de la commande.
Ce qui me permet de voir que le \siecle{i} siècle s'écrit un peu différemment des autres siècles.
Il est toujours en petites capitales, mais il y a un exposant différent.
Aucun problème, j'ai juste à faire un test pour connaitre le numéro du siècle à afficher et afficher le bon exposant en fonction du résultat du test.
En gros, il faut moins d'un quart d'heure pour écrire une commande en prenant son temps.
Mais une fois, ce qui vous paraît comme une perte de temps, passé, je peux l'utiliser autant de fois que je veux dans tout mon document.
Et même dans tous mes documents.

Et c'est là le second avantage de la séparation du fond et de la forme.
C'est que ma commande peut être utilisée dans tous mes documents et même dans les documents d'autres personnes.
Et là, c'est beau.
Par exemple, il se pourrait que je sois incapable d'écrire la moindre commande mais que je connaisse quelqu'un capable d'en écrire.
J'écris une commande vide que j'utilise dans mon document et je continue à le rédiger.
En même temps, je vais demander à quelqu'un qui sait comment faire.
J'envoie un fichier avec les descriptions des commandes, la personne rempli mon fichier, me le renvoie complété, je l'intègre et c'est tout bon.
Si j'ai besoin d'aide pour la rédaction d'une commande, un simple copier/coller de la réponse marche.
Avec Word, c'est plus délicat.
Il faut commencer par connaître la version de Windows, les paramètres régionaux, la version de Word et probablement la couleur du fond d'écran.
Et ensuite, avec quarante deux captures d'écran, nous avons réussit à résoudre notre problème.
La réponse apportée marche bien\ldots\ jusqu'à la prochaine mise à jour de Word où tous les menus ont été chamboulés.

Oui, parce qu'un autre intérêt de la séparation du fond et de la forme, c'est pour le suivi.
Parce que s'il s'avère qu'une commande ne fonctionne plus à cause d'une mise à jour, ce n'est pas un gros problème avec \LaTeX{}.
D'abord, je n'ai jamais vu ce cas se produire, mais c'est envisageable.
Il n'y a que dans la définition de la commande qu'il faut faire une modification.
De même, si en plein milieu de la rédaction de mon document j'ai une nouvelle idée.
Par exemple, de changer la police de caractère utilisée pour l'affichage du numéro du siècle.
Je le fais juste dans la définition de ma commande, pas dans tout mon document.
Alors qu'avec Word, il m'aurait fallu rechercher tous les siècles pour les modifier un par un.

Je sais, avec Word, il existe des feuilles de style.
D'abord, pratiquement personne ne sait les utiliser en dehors des titres.
C'est donc que ça n'a pas la simplicité annoncée par ceux qui disent que Word est plus simple que \LaTeX{}.
Ensuite, ce que j'en ai vu semble à peu près correct pour les titres, mais il ne faut pas en demander trop non plus.
Par exemple, quand j'ai vu le service de communication donner sa nouvelle charte graphique.
Les collègues qui devaient modifier leurs anciens documents riaient beaucoup moins que moi.
Si je riais, c'est parce que je n'avais pas de document à modifier.
Pour aller plus loin, je parle des siècles parce que c'est un exemple que je trouve intéressant.
Mais quelqu'un qui fait un document sur la musique pourrait apprécier d'avoir des commandes \liglat{\auteur{}} et \liglat{\instrument{}} par exemple pour les mettre en évidence.
Le rédacteur de ce document pourrait aimer avoir un index listant les auteurs et un autre index listant les instruments.
Il est super simple de demander à \LaTeX{} d'ajouter automatiquement l'auteur ou l'instrument dans le bon index à chaque appel de la commande.
La même chose avec les feuilles de style de Word, au mieux, c'est réservé aux super experts.
Bref, la séparation du fond et de la forme permet de faire évoluer la forme le plus simplement possible.
Word ne me permet pas de faire la même chose aussi simplement.

\section{La gestion de documents}
J'ai déjà un peu abordé le sujet, mais maintenant, je vais y aller à fond.
Lorsque j'écris un document que je considère comme important, je veux qu'il dure.
Avec Word, pendant combien d'années pouvez-vous continuer à ouvrir un document ?
Et en rajoutant en plus un espoir de ne pas trop y perdre dans la mise en page que vous vous étiez énervé à peaufiner ?
Vous allez me dire, que ça dépend du nombre de versions de Word et de la complexité du document.
Mais dans l'ensemble, espérer conserver un document Word pendant dix ou quinze ans sans y toucher est illusoire.
Le mieux est de l'ouvrir avec la dernière version de Word et de l'enregistrer dans le nouveau format pour ne pas tout perdre.
À chaque changement de version.
Parce que si vous n'arrivez pas à l'ouvrir, vous perdez tout.
Alors qu'avec \LaTeX{}, comme le source est du texte brut, même si \LaTeX{} disparaît, je pourrai toujours lire mon source.
Quoiqu'il arrive d'ici trente ou quarante ans, je pourrai toujours avoir accès au fond de mon document (sauf si je meurs avant, mais là n'est pas la question).
Moyennant quelques modifications, je serai même capable de l'afficher sous un autre format, du html par exemple.
Ou un autre format auquel je ne peux pas penser parce qu'il n'existe pas encore mais qui sera mieux.
\LaTeX{} m'apporte donc un confort inimaginable sur Word quant à la pérennité de mes documents.

Mais bon, un document que je considère important, c'est beau, mais il y a deux points à prendre en compte.
D'abord, s'il est important, il ne faut pas que je le perde.
Je dois donc le stocker à plusieurs endroits.
Imaginez que mon ordinateur lâche et que je n'ai pas de sauvegarde, je perds tout en quelques secondes.
Il faut donc qu'il soit sur mon ordinateur, sur au moins une clé USB et sur le plus d'endroits possibles.
Ensuite, un document important ne s'écrit pas en trois minutes.
Sinon, en cas de perte, il n'y a pas de problème, il me suffit de le réécrire.
Et donc, qui dit document long à écrire, dit document qui évolue.
Vous avez donc un document situé à plusieurs endroits, mais qui n'a pas évolué partout de la même façon.
Par exemple, lorsque vous faites évoluer le document sur votre ordinateur.
Au moment où vous l'enregistrez, il n'est pas dans le même état que ceux qui sont sur vos sauvegardes.
Il faut donc bien s'y prendre pour gérer les versions du document principal et ses sauvegardes.

La façon la plus basique est de mettre la date dans le nom du document.
Ou alors, de mettre les documents relatifs dans un répertoire avec une date dans son nom.
Lorsque votre document arrive à un état stable et que vous voulez le faire évoluer, vous recopiez le répertoire en changeant la date.
Comme ça, si vous n'êtes pas satisfait de l'évolution, vous revenez sur le répertoire précédent.
Mais il y a une limite, c'est pour connaitre la différence entre les deux versions.
Il y a bien la possibilité d'ouvrir vos documents et de les comparer ligne à ligne.
C'est pas glorieux comme solution, surtout si les documents sont très longs et que vous n'avez aucun outil pour vous aider.
Pour jouer, nous allons complexifier la tâche.
Imaginons que nous soyons plusieurs à faire évoluer le même document.
Lorsque deux personnes ont fait évoluer le document, je ne connais pas de méthode simple pour récupérer les évolutions des deux personnes dans le même document.
Dans les entreprises, c'est un cas fréquent et pour cela, il a été développé la «~GED~».
C'est un acronyme, il se prononce donc «~gède~» et signifie Gestion Électronique de Documents.
Le technique est de mettre le document dans un espace commun entre les utilisateurs.
Lorsqu'un utilisateur veut modifier son document, il bloque l'accès pour empêcher les autres utilisateurs de le modifier.
C'est tellement limité comme solution que j'en rie encore.
Je veut dire qu'au moment où j'écris ces lignes, je suis en train de rire, mais au moment où vous les lisez je suis encore en train d'en rire.

C'est là que Git (c'est un «~G~» dur et toutes les lettres se prononcent, ça se prononce donc «~guite~») entre en jeu.
Il est à l'origine créé pour les développeurs qui avaient besoin de gérer les versions de Linux.
De la même façon que \LaTeX{} utilise des fichiers textes pour créer un beau document à lire, les développeurs utilisent un fichier texte pour avoir un logiciel.
Et donc, la gestion des sources des document \LaTeX{} étant la même que la gestion des sources des logiciels, il est possible d'utiliser Git pour gérer les sources de \LaTeX{}.

Et là, toutes les réponses aux questions précédemment posées apparaissent.
Je veux gérer mon document avec Git ?
C'est facile, je vais dans mon répertoire et j'utilise la commande \liglat{git init}.
Je veux faire une sauvegarde ?
Je vais sur ma clé usb et j'utilise \liglat{git clone}, et hop, j'ai une sauvegarde sur ma clé usb qui est gérée par git.
Je veux enregistrer mes modifications en indiquant que j'atteins une étape à noter ?
Je fais un petit \liglat{git commit} et j'ai deux versions différentes dans mon même répertoire.
Je peux basculer d'une version à l'autre avec un petit \liglat{git checkout}.
Je veux comparer deux versions d'un document, récupérer les modifications faites par quelqu'un d'autre et intégrer ses modifications dans mon document ?
Rien de plus simple, \liglat{git diff}, \liglat{git fetch} et \liglat{git merge} sont là pour ça.
Quand je parle de comparaison, il m'affiche tous les fichiers qui ont été modifiés.
Avec pour chaque fichier, les lignes ajoutées, les lignes enlevées et les lignes modifiées.
Par rapport à la comparaison des documents Word où vous devez comparer toutes les lignes sans aucune aide, ici, vous ne comparez que les modifications.
L'apport est énorme.
Il m'est possible d'accepter une modification et d'en refuser une autre.
Bref, que du bonheur.

Imaginons que je sois en vacances et que je fasse lire un texte à quelqu'un.
La réponse est sans appel, il y a une faute d'orthographe ou une phrase pas claire.
Avec word, il faut que je sois sûr d'avoir la dernière version sur ma clé usb.
Il faut aussi que la personne ait la même version de Word pour ne pas perdre ma mise en page (et encore, ce n'est pas forcément suffisant en fonction des pays et des langues des utilisateurs j'ai déjà eu des surprises).
Si toutes les conditions ne sont pas réunies, j'ai besoin d'un pense-bête.
Avec \LaTeX{}, aucun problème.
Je modifie mon source tout de suite sur ma clé usb et je n'y pense plus.
S'il peut lire ma clé usb, j'ai forcément la possibilité de modifier mon source.
Que ce soit avec un bloc note Windows ou un notepad dans le pire des cas, ou avec un éditeur plus évolué, c'est possible.
Une fois chez moi, \liglat{git commit}, \liglat{git fetch} et \liglat{git merge} et c'est fini.
Même si je n'ai pas la dernière version de mon document sur ma clé usb, Git m'aidera à intégrer les modifications proprement.
Comme Git se contente d'ajouter un répertoire pour lui dans mon répertoire de travail, la personne n'a pas besoin d'avoir Git pour que ce soit pris en compte.
Il suffit de modifier mon document \LaTeX{} pour que Git prenne les modifications en compte une fois chez moi.
Il n'est pas possible de gérer un document Word avec Git.
Parce qu'un document Word est un document qui intègre à la fois du texte et de la mise en page sous forme binaire\Note{Ce n'est plus tout à fait vrai avec le format docx qui peut être utilisé par les dernières versions de Word.}.
Git est capable d'extraire le contenu d'un document Word, mais je perds la mise en page.
Word m'empêche donc de gérer mes documents de façon simple.

Et la petite cerise sur le gâteau, c'est pour la mise à jour.
En plus de mes sauvegardes, j'utilise gitlab qui m'offre un espace partagé sur Internet.
Lorsque j'arrive à un résultat que je veux publier, je mets mes modifications sur gitlab.
C'est simple, un petit \liglat{git push} fait l'affaire.
Mais en plus, une fois que j'ai déployé ma version, il compile lui-même mon document pour que le fichier pdf soit téléchargeable sur mon site Internet.
Vous pouvez automatiquement récupérer la dernière version de mon document sans que je n'ai rien à faire de spécial.
Allez expliquer à Word que quand vous voulez le rendre public il doit se déplacer lui même sur votre site Internet.
Quand vous y arriverez, nous en reparlerons et vous comprendrez la puissance de Git.

Vous allez me dire que c'est compliqué et que ce sont des commandes à écrire ?
D'abord, le fait que ce soient des commandes permet des automatisations simples.
Ensuite, il est possible de n'écrire que le début des commandes, des mécanismes aident à les compléter.
Enfin, il existe des interfaces graphiques pour faire la même chose et éviter d'écrire des commandes.
Mais je préfère me passer de la souris, je vais expliquer pourquoi maintenant.
