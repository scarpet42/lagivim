p  LAGIVIM

Ce projet est destiné à produire un document LaTeX assez rapide à lire.

## Raison

Je suis fatigué de répéter, depuis des années, que Word© est médiocre.
Tout le monde utilise Word©, si je critique, certains considèrent donc que c'est pour ne pas faire comme tout le monde.
Surtout que LaTeX semble beaucoup plus difficile à utiliser et que ça ne sert à rien de se compliquer la vie juste pour être différent.

Plutôt que de toujours répéter la même chose, j'ai décidé, il y a quelques années, de donner mes arguments une bonne fois pour toutes.
Je me suis énervé à rédiger un document montrant :

*   la médiocrité de Word,
*   le principe de vim, de git et le LaTeX,
*   en quoi ça me fait vraiment gagner du temps par rapport à Word.

## Pré-requis

Je considère que les gens qui lisent ce document connaissent Word, mais n'ont jamais entendu parler de Git, de vim ou de LaTeX (ou seulement des bribes destinées à faire peur).
Le but du document compilé est d'expliquer pourquoi je préfère utiliser Git + LaTeX + vim que Word.
Il n'y a donc pas de pré-requis : n'importe qui doit être capable de le comprendre.
Si ce n'est pas le cas, merci de me prévenir que je le clarifie.

## Ce qu'il n'est pas

Ce document n'est en _aucun cas_ une initiation à LaTeX, vim ou Git.
Le lecteur comprendra leurs intérêts mais ne saura pas les utiliser.

Ce document n'est en _aucun cas_ destiné à obliger les gens à arrêter d'utiliser Word©.
Chacun fait ce qu'il veut, j'explique pourquoi mon choix est le bon, même si tout le monde peut faire des choix différents.

Ce document n'est en _aucun cas_ exhaustif. Il est destiné à être lu en moins d'une heure : il y a beaucoup d'autres choses à dire.
Mais si ce document n'a pas convaincu, il n'est pas utile d'en rajouter, rien de plus ne saurait convaincre.

## Lecture du document compilé
Le document compilé est disponible ici : [lagivim.pdf](https://scarpet42.gitlab.io/lagivim/lagivim.pdf)

## Informations complémentaires

Je découvre gitlab avec ce document.
Vous verrez des erreurs de compilation dans l'historique car il a fallu que je résolve les problèmes en découvrant son utilisation.
La difficulté n'est pas dans la compilation de LaTeX en soit, mais dans l'utilisation d'une chaine d'intégration continue à la fois nouvelle et externe.

## License

La licence est la WTFPL : [WTFPL](http://www.wtfpl.net/)
C'est la plus permissive des licences.
Si je ne le fais pas, vous n'avez rien le droit de faire du texte sans mon accord.
